# Kotlin Spring JPA

### Run locally

```
git clone https://gitlab.com/hendisantika/kotlin-spring-jpa.git
```

```
mvn clean spring-boot:run
```

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

List All Data

![List All Data](img/list.png "List All Data")

Count Data

![Count Data](img/count.png "Count Data")